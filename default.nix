{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/8542021fe7f9f777294649b1cdc853240806ff21.tar.gz") {}
, mkDerivation ? pkgs.stdenv.mkDerivation
, fetchzip ? pkgs.fetchzip
, pkg-config ? pkgs.pkg-config
, alsaLib ? pkgs.alsaLib
, gtk2-x11 ? pkgs.gtk2-x11
, gtksourceview ? pkgs.gnome2.gtksourceview
, libao ? pkgs.libao
, libGL ? pkgs.libGL
, libpulseaudio ? pkgs.libpulseaudio
, libudev ? pkgs.libudev
, libX11 ? pkgs.xorg.libX11
, libXext ? pkgs.xorg.libXext
, libXrandr ? pkgs.xorg.libXrandr
, libXv ? pkgs.xorg.libXv
, openal ? pkgs.openal
, SDL2 ? pkgs.SDL2
}:
mkDerivation rec {
  pname = "ares";
  version = "v118";

  src = fetchzip {
    url = "https://ares.dev/downloads/${pname}_${version}-source.zip";
    sha256 = "0fkd9ynlkrx2y42bfqz24lbbkgjqb3ggxjn6dxb1q2z4cffcw7l5";
  };

  buildInputs = [ pkg-config ];
  nativeBuildInputs = [ alsaLib gtk2-x11 gtksourceview libao libGL libpulseaudio libudev libX11 libXext libXrandr libXv openal SDL2 ];

  dontConfigure = true;

  buildPhase = ''
    sed -i 's@HOME)/\.local@out)@' "nall/GNUmakefile"
    cd lucia
    make install
  '';

  dontInstall = true;
}
